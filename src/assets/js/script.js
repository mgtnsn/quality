// 共通：ページ内スクロール
const speed = 500;
let href,target,position;
$('a[href^="#"]').click(function(e){
  if($(this).attr('href')!=='#'){
    href= $(this).attr("href");
    target = $(href == "#" || href == "" ? 'html' : href);
    position = target.offset().top;
    $('body,html').animate({scrollTop:position}, speed, 'swing');
  }
});

// 新着案件：カルーセル
$('#js-slick-latest').slick({
  arrows: false,
  autoplay: true,
  autoplaySpeed: 5000,
  pauseOnHover: true,
  mobileFirst: true,
  centerMode: true,
  centerPadding: '25px',
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        variableWidth: true,
        centerPadding: '50px',
      }
    }
  ]
});