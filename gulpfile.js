'use strics';

const browsersync = require('browser-sync');
const gulp        = require('gulp');
const rename      = require('gulp-rename');
const sass        = require('gulp-sass')(require('sass'));
const postcss     = require('gulp-postcss');
const csssorter   = require('css-declaration-sorter');
const mqpacker    = require('css-mqpacker');
const imagemin    = require('gulp-imagemin');
const uglify      = require('gulp-uglify');

const path = {
  'dist': {
    'root' : 'dist/',
    'css'  : 'dist/assets/css/',
    'img'  : 'dist/assets/img/',
    'js'   : 'dist/assets/js/'
  },
  'src': {
    'root' : 'src/',
    'css'  : 'src/assets/scss/',
    'img'  : 'src/assets/img/',
    'js'   : 'src/assets/js/'
  },
}

// server
function server(done){
  browsersync.init({
    server: path.dist.root,
  });
  done();
}

// reload
function reload(done){
  browsersync.reload();
  done();
}

// css
function css(){
  return gulp.src(path.src.css+'**/*.scss')
  .pipe(sass({
    // expanded or compressed
    outputStyle: 'compressed'
  }))
  .pipe(postcss([
    csssorter({
      order: 'smacss'
    }),
    mqpacker({
      sort: true
    })
  ]))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(path.dist.css))
}

// js
function js(){
  return gulp.src(path.src.js+'**/*.js')
  .pipe(uglify())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(path.dist.js))
}

// image
function img(){
  return gulp.src(path.src.img+'**/*.+(png|jpg|gif|svg)')
  .pipe(imagemin([
	  imagemin.optipng({optimizationLevel: 7}),
    imagemin.mozjpeg({quality: 70, progressive: true}),
    imagemin.svgo(),
    imagemin.gifsicle()
  ]))
  .pipe(gulp.dest(path.dist.img))
}

// watch
function watch(done){
  gulp.watch(path.src.css+'**/*.scss', css);
  gulp.watch(path.src.js+'**/*.js', js);
  gulp.watch(path.src.img+'**/*.+(png|jpg|gif|svg)',img);
  gulp.watch(path.dist.root+'**/*', reload);
  done();
}

exports.default = gulp.series(gulp.parallel(css, js, img), server, watch);