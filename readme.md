# README

アライブ株式会社様 コーディングテスト用のリポジトリです。

## 開発環境

当リポジトリでは、タスクランナー「Gulp」を使い下記の機能を実装しています。

* ブラウザの自動リロード
* Sassのコンパイル&圧縮
* JSの圧縮
* 画像の圧縮

### ローカルサーバの起動

`dist/`をルートディレクトリとしてローカルサーバを起動します。

```
# モジュールのインストール(初回のみ)
npm install

# タスク実行&ローカルサーバ起動
# Ctrl(Cmd)+C で停止
npx gulp
```

#### 注

圧縮後のCSS,JS,画像はGit管理対象外としているため、上記`npx gulp`の実行が必須です。  

成果物だけをご覧になりたい場合はそのように修正しますので、お手数ですがご連絡ください。  
もしくは、下記URLからご確認いただくこともできます。  
https://www.mgtnsn.com/alive/quality/  
ID: alive  
Pass: pT3NWd9b


### 動作確認環境

* Node.js ... 16.13.0
* npm ... 8.1.0